'use strict';
class XOItem {
  constructor(url, displayText, iconNumber, /*XOItem[]*/ children) {
      this.url = url;
      this.displayText = displayText;
      this.children = children;
      this.TypeLabel = "XOItem";
      this.iconNumber = iconNumber;

      // Set children's parent to this instance
      var thisItem = this;
      if (this.children) {
        $.each(this.children, function(i, val) {
          val.parent = thisItem;
        });
      }
    }
    
    /*
      constructor(url, displayText) {
        this.children = [];
        this.url = url;
        this.displayText = displayText;
      }*/

  addChild( /*XOItem*/ childElmt) {
    this.children.push(childElmt);
    childElmt.parent = this;
  }

  renderChildrenHtml(parentElmt) {
    if (this.children) {
      var newUL = $("<ul>").appendTo(parentElmt);

      // Call createHtml for children
      $.each(this.children, function(i, val) {
        val.renderHtml(newUL);
      });

      $("</ul>").appendTo(parentElmt);
    }
  }

  // Render the XOItem as html LI element
  renderHtml(parentElmt) {
    var getText = this.displayText;
    console.log(getText);
    var html = "<li>" 
    + "<span>"
    //+ "<span class='TypeLabel'>" + this.TypeLabel + "</span>"
    //+ "<i class='" 
    //+ this.constructor.name 
    //+ " icon-folder-open'></i> "
    + "<img src='" + chrome.extension.getURL("/icons/" + this.iconNumber + ".png") + "'/> "
    + "<a href='https://i-00629072b54d40642.workdaysuv.com/super/d/inst/1$17/2997$817.htmld'>" 
    + getText
    + "</a>"
    + "</span></li>";
    
    var newLI = $(html).appendTo(parentElmt);

    this.renderChildrenHtml(newLI);

    /* $('<div/>', {
        id: 'foo',
        href: 'http://google.com',
        title: 'Google',
        rel: 'external',
        text: 'test'
    }).appendTo('#tree'); */
  }
}

class XORootNode extends XOItem {
  renderHtml(parentElmt) {
    // Just render children
    this.renderChildrenHtml(parentElmt);
  }
}

class XOSession extends XOItem {
	
  //renderHtml() {}
}

class XOMethod extends XOItem {
  //this.TypeLabel = "ME";
  //renderHtml() {}
}

class XOMethodBinding extends XOItem {
  //this.TypeLabel = "MB";
  //renderHtml() {}
}

class XOElement extends XOItem {
  //this.TypeLabel = "EL";
  //renderHtml() {}
}

class XOElementContent extends XOItem {
  //this.TypeLabel = "EC";
  //renderHtml() {}
}

class XOTask extends XOItem {
//  constructor(url, displayText, /*XOItem[]*/ children) {
//  	super();
//  	this.TypeLabel = "TASK";
//    this.iconNumber = 0;
//  }
  //renderHtml() {}
}
var firstHref;
var title;

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if( request.message === "clicked_hyperlink" ) {

      //Print the URL to the console	
      firstHref = document.URL;
      console.log(firstHref);

      //Extract the class for XO icons
      var last_$ = firstHref.lastIndexOf('$');
      var last_slash = firstHref.lastIndexOf('/');
      if (last_$ > 0){

        var instance = firstHref.substring(last_slash + 1, last_$);
        console.log('/Icons/' + instance + '.png');
      }


      if(document.getElementsByClassName("gwt-Label WHXH")[1]){
        title = document.getElementsByClassName("gwt-Label WHXH")[1].title;
        console.log("TITLE CALLED")
      }
      
      if(title){
        console.log("Title: "+title);  
      }
      console.log("tab id: " + request.id);
      Render();
  	}
  }
  
);  


// Build some test objects for demo.
function buildDemoDataNode() {
  return new XORootNode(null, "ROOT", 0, [
    new XOTask("http://myurl.com", "Correct My Time Off, 2997$817", 86, [
      new XOMethod("http://myurl.com", "Correct My Time Off Start, 6$8107", 6, [
        new XOMethodBinding("http://myurl.com", "Worker@ Correct My Time Off Start(CT)*S, 8$2370", 8, null)
      ]),
      new XOMethod("http://myurl.com", "Correct Time Off Edit, 6$2535", 6, [
          new XOMethodBinding("http://myurl.com", "Correct Time Off Edit/a1/Worker for Time Off Correction Event Secured [WS]*ST*PR*FC, 56$7741", 22, null),
          new XOMethodBinding("http://myurl.com", "Correct Time Off Edit/b/Correct Time Off Subedit [EL], 56$7747", 22, null)
      ])
    ])
  ]);
}
/*
level 0
https://i-077a4cc755cc530fd.workdaysuv.com/super/d/inst/1$17/2997$817.htmld
/Icons/2997.png
[sequence task]: Correct My Time Off, 2997$817
    
    
        level 1
        https://i-077a4cc755cc530fd.workdaysuv.com/super/d/search.htmld?q=6$8107
        /Icons/6.png
        [el]: Correct My Time Off Start, 6$8107
                level 2
                https://i-077a4cc755cc530fd.workdaysuv.com/super/d/inst/3$12/8$2370.htmld
                /Icons/8.png
                [ct]: Worker@ Correct My Time Off Start(CT)*S, 8$2370
        level 1
        https://i-077a4cc755cc530fd.workdaysuv.com/super/d/search.htmld?q=6$2535
        /Icons/6.png
        [el]: Correct Time Off Edit, 6$2535
                level 2
                https://i-077a4cc755cc530fd.workdaysuv.com/super/d/inst/1$56/56$7741.htmld
                /Icons/56.png
                [ec]: Correct Time Off Edit/a1/Worker for Time Off Correction Event Secured [WS]*ST*PR*FC, 56$7741
                level 2
                https://i-077a4cc755cc530fd.workdaysuv.com/super/d/inst/1$56/56$7747.htmld
                /Icons/56.png
                [ec]: Correct Time Off Edit/b/Correct Time Off Subedit [EL], 56$7747

*/

/*
function buildDemoTree()
{
    var obj1 = {url: 'A', displayText: 'some display text'};
    var obj2 = {url: 'B', displayText: 'some display text'};
    var obj3 = {url: 'C', displayText: 'some display text'};
    var obj4 = {url: 'D', displayText: 'some display text'};
    
    
    addToRoot(obj1);
    //addChildNode("A",obj2);
    
    console.log(obj1);
}

// Method to be called to render tree.
function RenderTree(tree)
{
    
}
*/

// MAIN ////////
$(function() {
    
    //buildDemoTree();
//This function should be a variable so it can be called in the listener  
//var Render = function() {
  //$("#drawer").append("<link rel='stylesheet' type='text/css' href='foxo-ui.css'>");
    var style = document.createElement('link');
style.rel = 'stylesheet';
style.type = 'text/css';
style.href = chrome.extension.getURL('foxo-ui.css');
//(document.head||document.documentElement).appendChild(style);
(document.head).appendChild(style);
    
    //$("#drawer").append(style);

    
    // Get demo data
  var xoRootNode = buildDemoDataNode();

  
  // Render as HTML
  xoRootNode.renderHtml($("#drawer")); // $(".tree"));

    /*
  $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
  $('.tree li.parent_li > span').on('click', function(e) {

    e.stopPropagation();
  });
*/
});
/*
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if( request.message === "clicked_hyperlink" ) {

      //Print the URL to the console	
      var firstHref = document.URL;
      console.log(firstHref);

      //Extract the class for XO icons
      var last_$ = firstHref.lastIndexOf('$');
      var last_slash = firstHref.lastIndexOf('/');
      if (last_$ > 0){

        var instance = firstHref.substring(last_slash + 1, last_$);
        console.log('/Icons/' + instance + '.png');
      }

      var title;

      if(document.getElementsByClassName("gwt-Label WHXH")[1]){
        title = document.getElementsByClassName("gwt-Label WHXH")[1].title;
      }
      
      if(title){
        console.log(title);  
      }
  	}
  }
);  

*/

