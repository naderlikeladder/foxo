var tree;
var rootNode;
var currentURL;
// Called when the user clicks on a link inside an SUV
chrome.tabs.onUpdated.addListener(function(tabID, info, tab) {
	if(info.status == "complete"){
		// Send a message to the active tab
        if (!rootNode) {
            tree = new TreeModel();
            rootNode = tree.parse({
               url: tab.url,
                })
            console.log("TREE: ");
            console.log(rootNode);
            currentURL = tab.url;
        }
        else {
            addChildNode(currentURL, {"url": tab.url, "text": "Some text"});
            currentURL = tab.url;

        }
  		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    		var activeTab = tabs[0];
    		chrome.tabs.sendMessage(activeTab.id, {"message": "clicked_hyperlink", 
                                                   "id": activeTab.id,});
 //           chrome.tabs.sendMessage(activeTab.id, {"message": activeTab.id});

  		});
            console.log(rootNode);


  	}
});
// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function(tab) {
  // Send a message to the active tab
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    var activeTab = tabs[0];
    chrome.tabs.sendMessage(activeTab.id, {"message": "clicked_browser_action"});
  });
});

// Create a new tree node when a new window is opened.
/* chrome.windows.onCreated.addListener(function(){
	
		tree = new TreeModel();
		rootNode = tree.parse({
		   url: 'A-Root',
		});

		console.log("TREE: ");
		console.log(rootNode);

	}
);
*/
var getRootNode = function(){
	return rootNode;
}

var addToRoot = function(data){
	
	var childNodeData = data;
	childNodeData.parent = rootNode;
	
	var childNode = tree.parse(childNodeData);

	rootNode.addChild(childNode);

	console.log("Adding child Node to Root");

	return rootNode;
}

var addChildNode = function(parentUrl, data){
	// Parse the new node
	var parentNode = findNodeById(parentUrl);
	if(!parentNode){
		console.log("No Parent Node found, call addToRoot instead");
		return;
	}
	var childNodeData = data;
	//childNodeData.parent = parent;

	var childNode = tree.parse(childNodeData);
	// Add it to the parent
	parentNode.addChild(childNode);	
	console.log("Adding Child Node");

	return rootNode;
}

var removeChild = function (url){
	var node = findNodeById(url);
	if(!node){
		console.log("No Node found, cannot remove");
		return;
	}
	node.drop();

	return rootNode;
}

var findNodeById = function(url){
	var url2=url;
	var node = rootNode.first(function (node) {

	   return node.model.url === url2;
	});
	if(! node){
		console.log("Node Not Found");	
		return;
	}
	
	console.log(node)
	return node;
}

var printNodePath = function(url) {

	var node = findNodeById(url);

	console.log("Node path: " + node.getPath());
}
