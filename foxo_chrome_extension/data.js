// var parentNode = {
// 	displayText: "Parent Node",
// 	url: "test url",
// 	parent:
// 	isRoot: 1,
// 	children: []
// }

// var nodeObjects = [];


// var addChild = function(node, url){
// 	var childNode = {}
// 	childNode.parent = node;
// 	childNode.url = url;
// 	node.children.push(childNode);
// }


var tree = new TreeModel();
var rootNode;
var display = function(){
	//tree = new TreeModel();

	rootNode = tree.parse({
	   url: 'A-Root',
	   children: [
	   		{url: 'B', displayText: 'some display text',children: [{url: 'M'}, {url: 'N'}]},
	   		{url: 'C', displayText: 'some display text'},
	   		{url: 'D', displayText: 'some display text'},
	   		{url: 'E', displayText: 'some display text'}

	       // {
	       //     url: "",
	       //     children: [{url: 111}]
	       // },
	       // {
	       //     id: 12,
	       //     children: [{url: 121}, {url: 122}]
	       // },
	       // {
	       //     url: 13
	       // }
	   ]
	});

	console.log("TREE: ");
	console.log(rootNode);

}
var addToRoot = function(data){
	
	var childNodeData = data;
	childNodeData.parent = rootNode;
	
	var childNode = tree.parse(childNodeData);

	rootNode.addChild(childNode);

	console.log("Adding child Node to Root");
	//console.log(childNode);
}

var addChildNode = function(data){
	// Parse the new node
	var parentNode = findNodeById(data.url);
	if(!parentNode){
		console.log("No Parent Node found, call addToRoot instead");
		return;
	}
	var childNodeData = data;
	childNodeData.parent = parent;

	var childNode = tree.parse(childNodeData);
	//node124 = tree.parse({url: url, text: displayText});

	//node123 = tree.parse({id: 123, children: [{id: 1231}]});
	// var node12 = rootNode.first(function (node) {
	//    return node.model.id === 12;
	// });
	// Add it to the parent
	parentNode.addChild(childNode);	
	console.log("Adding Child Node");
	//console.log(childNode);
}

var removeChild = function (url){
	var node = findNodeById(url);
	if(!node){
		console.log("No Node found, cannot remove");
		return;
	}
	node.drop();
}

var findNodeById = function(url){
	var url2=url;
	var node = rootNode.first(function (node) {
	   //console.log(node.model.url);
	   return node.model.url === url2;
	});
	if(! node){
		console.log("Node Not Found");	
		return;
	}
	
	console.log(node)
	return node;
}

var printNodePath = function(url) {

	var node = findNodeById(url);

	console.log("Node path: " + node.getPath());
}

